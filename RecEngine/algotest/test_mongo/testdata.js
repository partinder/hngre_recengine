/**
 * Created by Partinder on 3/10/15.
 */

test_set = []

module.exports = function(org_matrix,users,dishes,number){

    var random_users = []
    for(i=0;i<number;i++)
    {
        random_users.push(users[Math.floor(Math.random()*users.length)])
    }

    for(i=0;i<random_users.length;i++){

        var test_dishes = []
        for(u=0;u<dishes.length;u++){
            if(org_matrix[i][u] > 0)
            {
                test_dishes.push({
                    user: random_users[i],
                    dish: dishes[u],
                    rating: org_matrix[i][u]
                })

                //org_matrix[users.indexOf(user)][dishes.indexOf(dish)] = 0

                //console.log("Dish id: %s, User id: %s, has rating: %s",dish,user, org_matrix[users.indexOf(user)][dishes.indexOf(dish)])
                //console.log(test_dishes)
            }

        }
        var random_test_dish = Math.floor(Math.random()*test_dishes.length)
        test_set.push(test_dishes[random_test_dish])
        org_matrix[users.indexOf(test_dishes[random_test_dish].user)][[dishes.indexOf(test_dishes[random_test_dish].dish)]] = 0

    }

    //test_set.forEach(function(set){
    //    console.log("Dish id: %s, User id: %s, Rating: %s, new rating : %s", set.dish, set.user,set.rating, org_matrix[users.indexOf(set.user)][dishes.indexOf(set.dish)])
    //})
    //console.log(test_set)
    return {org_matrix:org_matrix,test_set:test_set}

}

