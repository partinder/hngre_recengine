/**
 * Created by Partinder on 2/27/15.
 */
/**
 * Created by Partinder on 2/27/15.
 */

/*
 RATING
 Model and methods associated with ratings.
 */

var Mongoose = require('mongoose');

var RatingSchema = new Mongoose.Schema
({

});

//RatingSchema.index ({dish: 1, user: 1}, {unique: true, dropDups: true});
//The index above makes sure that only one rating exists per dish for a user.

RatingSchema.statics.findForDish = function (dishId, callback) {

    var query = this.find({dish: dishId}).populate('user', 'name');

    if (callback) {
        query.exec(function(err, ratings) {
            if (err) {
                return callback (err);
            } else {
                callback(null, ratings);
            }
        });
    } else {
        return query;
    }
}

RatingSchema.statics.findForUser = function (userId, callback) {

    var query = this.find ({user: userId});
    query.populate ('dish', 'name');

    if (callback) {
        query.exec (function(err, ratings) {
            if (err) {
                return callback (err);
            } else {
                callback(null, ratings);
            }
        });
    } else {
        return query;
    }
}

RatingSchema.statics.countForUser = function (userId, callback) {

    console.log('Getting count for user ', userId);
    this.count ({user: userId}, function (err, count) {
        if (err) {
            return callback(err);
        } else {
            console.log('found count ', count);
            callback(null, count);
        }
    });

}

RatingSchema.statics.rateDish = function (rate, callback) {
    this.findOneAndUpdate({dish:rate.dish, user:rate.user}, {rating:rate.rating}, {upsert: true}, function (err, rating) {
        if (err) {
            callback (err);
            return;
        } else {
            callback (null, rating);
        }
    });
}

//RatingSchema.fetchUnrated = function (params, callback) {
//	//List restaurants near the user which have not yet been rated by him.
//
//}

var Rating = Mongoose.model('Rating', RatingSchema);

module.exports = Rating;

