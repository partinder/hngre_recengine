/**
 * Created by Partinder on 3/13/15.
 */

module.exports ={

    kItemSim : function (indexA, indexB, tempV) {
        //console.log("Made call",indexA,indexB)

    var numerator = 0;
    var denomLeft = 0;
    var denomRight = 0;

    for (var i = 0; i < tempV.length; i++) {
        var rA = tempV[i][indexA];
        var rB = tempV[i][indexB];

        numerator += rA * rB;
        denomLeft += Math.pow(rA, 2);
        denomRight += Math.pow(rB, 2);
    }

    var denominator = Math.sqrt(denomLeft + denomRight);
        //console.log("Item sim for %s,%s is %s",indexA,indexB,(numerator/denominator))

    return numerator / denominator;
},
    kUserSim : function (indexA, indexB, tempU) {
    //Similarity between two users by comparing predictions on k pseudo-items.

    var numerator = 0;
    var denomLeft = 0;
    var denomRight = 0;

    for (var i = 0; i < tempU[0].length; i++) {

        var rA = tempU[indexA][i];
        var rB = tempU[indexB][i];

        numerator += rA * rB;
        denomLeft += Math.pow(rA, 2);
        denomRight += Math.pow(rB, 2);
    }

    var denominator = Math.sqrt (denomLeft + denomRight);

    return numerator / denominator;
},
    userSim : function (userA, userB) {

    console.log('Calculating user similarity between', userA, userB);
    var items = [];
    //Pick up k items.
    items = itemIndex; //k is all items for now.

    var numerator = 0; //For sum(r(i, ua) * r(i, ub))
    var denomLeft = 0; //For sum(r(i, ua)^2)
    var denomRight = 0; //For sum(r(i, ub)^2)

    for (var i = 0; i < items.length; i++) {
        //Calculate rating for that item for both users.
        var ratingA = calcBasePrediction(userA, items[i]);
        var ratingB = calcBasePrediction(userB, items[i]);

        numerator += ratingA * ratingB;
        denomLeft += Math.pow(ratingA, 2);
        denomRight += Math.pow(ratingB, 2);
    }

    var denominator = Math.sqrt(denomLeft * denomRight);

    return (numerator / denominator);
}

}

