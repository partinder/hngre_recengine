/**
 * Created by Partinder on 6/6/15.
 */

var MongoClient = require("mongodb").MongoClient
var ObjectID = require('mongodb').ObjectID
var global = require("../Config/global")

MongoClient.connect("mongodb://"+global.db_server+"/db_hngre", function (err, db){
    if(err)
    {
        console.log(err)
    }
    else
    {
        var dumy_users = db.collection("dumy_users")

        var fs = require('fs');
        var users = []
        var array = fs.readFileSync('ASL.txt').toString().split("\n");
        for(i in array) {
            users.push(array[i])
        }

        asyncLoop(users.length,function(loop){
            dumy_users.update({_id:ObjectID(users[loop.iteration()].split(",")[0].toString())},{$set:{gender:users[loop.iteration()].split(",")[1],age:users[loop.iteration()].split(",")[2]}},function(err,count,status){
                if(err)
                {
                    console.log(err)
                }
                else
                {
                    console.log(users[loop.iteration()].split(",")[0],users[loop.iteration()].split(",")[1],users[loop.iteration()].split(",")[2])
                    console.log(status)
                    loop.next()
                }
            })
        },function(){
            console.log("All Users Updated")
        })
    }

})

function asyncLoop (iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}