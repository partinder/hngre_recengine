/**
 * Created by Partinder on 3/11/15.
 */
var Vector = require("sylvester").Vector
require("sylvester")
var dataops = require("../helpers/dataops")
var data = require("../pre/data").svd

module.exports = function(user_p,dish_p,userspace,dishspace){  // userspace and dishspace are regular matrix and not Sylvester Matrix

    //user_p and dishp is the index of the user in users and dish in dishes respectively

    var userRow = userspace[user_p];
    var dishcol = [];

    for (var i = 0; i < dishspace.length; i++) {
        dishcol.push(dishspace[i][dish_p]);
    }


    userRow = $M(userRow).transpose();
    dishcol = $M(dishcol);

    //var user_avg = dataops.user_avg(data.org_matrix,user_p,data.users,data.dishes)
    var prediction = (userRow.x(dishcol)).elements[0][0];

    return prediction

}