/**
 * Created by Partinder on 6/6/15.
 */

var merchants = require("../../Models/merchant").merchant
var rating = require("../../Models/ratingm")
var mongoose = require("mongoose")
var _ = require("underscore")
var data = require("../pre/data")
var mongoClient = require("mongodb").MongoClient
var ObjectID = require("mongodb").ObjectID
var global = require("../../Config/global")
var dataops = require("../helpers/dataops")

//mongoose.connect("mongodb://"+global.db_server+"/db_hngre")

module.exports = {
    create_clusters : function(callback){

        var startime = new Date().getTime()
        merchants.distinct("dishes.cuisine")
            .exec(function (err, res) {
                res.forEach(function (cui) {
                    //console.log(cui)
                    data.kmeans.cuisines.push(cui)
                })


                merchants.find({"status": "completed"})
                    .exec(function (err, res) {
                        res.forEach(function (merchant) {
                            merchant.dishes.forEach(function (dish) {

                                data.kmeans.knn_dishes.push(dish._id + "_" + merchant._id)

                                var temp = []

                                data.kmeans.taste_index.forEach(function (key) {
                                    temp.push(parseInt(dish.index[key]))
                                })
                                temp.push(parseInt(data.kmeans.cuisines.indexOf(dish.cuisine[0])))
                                temp.push(merchant.info.price_range.length)
                                //temp.push(dish.cuisine[0])
                                data.kmeans.vector_dishes.push(temp)
                            })

                        })

                        mongoClient.connect("mongodb://"+global.db_server+"/db_hngre",function(err,db){
                            if(err) throw err
                            var users = db.collection("users")
                            var dumy_users = db.collection("dumy_users")
                            //var ratings = db.collection("ratings")
                            var users_id = []
                            var user_vector = {}

                            users.find({"birthday":{$ne:null},"gender":{$ne:null}},{"gender": 1,"birthday":1}).toArray(function(err,res){

                                res.forEach(function(user){
                                    var gender = 0
                                    if(user.gender == "male" || user.gender == "m") gender = 1
                                    users_id.push(ObjectID(user._id))
                                    user_vector[user._id]={
                                        age:parseInt(new Date().getFullYear() - user.birthday.getFullYear()),
                                        gender: gender

                                    }

                                })

                                dumy_users.find({"age":{$exists:true},"gender":{$exists:true}},{"age": 1,"gender":1}).toArray(function(err,res){

                                    res.forEach(function(user){
                                        var gender =0
                                        if(user.gender == "male" || user.gender == "m") gender = 1
                                        users_id.push(ObjectID(user._id))
                                        user_vector[user._id]={
                                            age:parseInt(user.age),
                                            gender: gender

                                        }

                                    })

                                    rating.find({"_user":{$in:users_id},"rating":{$exists:true}})
                                        .distinct("_user")
                                        .exec(function (err, users_mongo) {
                                                if(err){
                                                    console.log("mongoose",err)
                                                }
                                                else{
                                                    //console.log(users_mongo.length)


                                                    users_mongo.forEach(function (final_user) {
                                                        //console.log(user_vector[final_user])
                                                        data.kmeans.vector_users.push([user_vector[final_user]["gender"], user_vector[final_user]["age"]])
                                                    })
                                                    log.info("Total Users to Cluster: %s",data.kmeans.vector_users.length)
                                                    log.info("Total Dishes to Cluster: %s",data.kmeans.vector_dishes.length)

                                                    var cluster_size_dishes = Math.ceil(Math.sqrt((data.kmeans.vector_dishes.length) / 2))
                                                    var cluster_size_users = Math.ceil(Math.sqrt((data.kmeans.vector_users.length) / 2))

                                                    var kmeans = require('node-kmeans');
                                                    var ml = require("machine_learning")

                                                    //Create Clusters dishes
                                                    data.kmeans.knn_cluster.dishes = ml.kmeans.cluster({
                                                        data: data.kmeans.vector_dishes,
                                                        k: cluster_size_dishes,
                                                        epochs: 200
                                                        // distance : {type : "pearson"}
                                                    })

                                                    //Create Clusters users
                                                    data.kmeans.knn_cluster.users = ml.kmeans.cluster({
                                                        data: data.kmeans.vector_users,
                                                        k: cluster_size_users,
                                                        epochs: 200
                                                        //distance : {type : "pearson"}
                                                    })

                                                    var dishes_index =[]
                                                    var users_index =[]


                                                    for(v=0;v<data.kmeans.vector_dishes.length;v++)
                                                    {
                                                        for(i=0;i<data.kmeans.knn_cluster.dishes.clusters.length;i++)
                                                        {
                                                            if(data.kmeans.knn_cluster.dishes.clusters[i].indexOf(v) != -1)
                                                            {
                                                                dishes_index.push(i)
                                                                break
                                                            }

                                                        }

                                                    }

                                                    for(var v=0;v<data.kmeans.vector_users.length;v++)
                                                    {
                                                        for(var i=0;i<data.kmeans.knn_cluster.users.clusters.length;i++)
                                                        {
                                                            if(data.kmeans.knn_cluster.users.clusters[i].indexOf(v) != -1)
                                                            {
                                                                users_index.push(i)
                                                                break
                                                            }


                                                        }

                                                    }

                                                    data.kmeans.cluster_index.dishes = dishes_index
                                                    data.kmeans.cluster_index.users = users_index

                                                    data.kmeans.knn_classify.dishes = new ml.KNN({
                                                        data : data.kmeans.vector_dishes,
                                                        result : data.kmeans.cluster_index.dishes
                                                    })

                                                    data.kmeans.knn_classify.users = new ml.KNN({
                                                        data : data.kmeans.vector_users,
                                                        result : data.kmeans.cluster_index.users
                                                    })

                                                    db.close()
                                                    var endtime = new Date().getTime()
                                                    log.info("Finished Clustering, took %s seconds", (endtime - startime) / 1000)
                                                    callback(true)

                                                    //callback(null,users_mongo)
                                                }
                                            })// Ratings Find

                                }) // Dumy Users
                            }) // Users
                        }) // Mongo Client
                    }) // Merchant Find
            }) // Cuisine

    },
    classify_users : function(db,user_id,callback){

        //mongoClient.connect("mongodb://"+global.db_server+"/db_hngre",function(err,db){
        //    if(err)
        //    {
        //        console.log(err)
        //    }
        //    else
        //    {
                //console.log(user_id)
                var users = db.collection("users")
                var dumy_users = db.collection("dumy_users")
                users.findOne({"_id":ObjectID(user_id)},{"gender":1,"birthday":1},function(err,res){

                    if(res == null)
                    {
                        dumy_users.findOne({"_id":ObjectID(user_id)},{"gender":1,"age":1},function(err,res){


                            var gender = 0
                            //console.log(res)
                            if(res.gender == "male") gender = 1
                            var user_vector = [gender,parseInt(res.age)]

                            //console.log(user_vector)
                            var cluster_index = data.kmeans.knn_classify.users.predict({
                                x : user_vector,
                                k: data.kmeans.cluster_size.users
                            })



                            var users =[]

                            var users_index = data.kmeans.knn_cluster.users.clusters[Math.floor(cluster_index)]
                            users_index.forEach(function(user_index){
                                users.push(user_index)
                            })
                            //db.close()
                            callback(Math.ceil(cluster_index),users)

                        })
                    }
                    else
                    {
                        var gender = 0
                        if(res.birthday == null || res.gender == null)
                        {
                            callback(null,null)
                        }
                        else
                        {
                            if(res.gender == "male") gender = 1
                            var user_vector = [gender,parseInt(new Date().getFullYear() - res.birthday.getFullYear())]
                            var cluster_index = data.kmeans.knn_classify.users.predict({
                                x : user_vector,
                                k: data.kmeans.cluster_size.users
                            })
                            var users =[]
                            var users_index = data.kmeans.knn_cluster.users.clusters[Math.floor(cluster_index)]
                            users_index.forEach(function(user_index){
                                users.push(user_index)
                            })
                            //db.close()
                            callback(Math.ceil(cluster_index),users)

                        }


                    }


                })
        //    }
        //})


    },
    classify_dishes : function(db,dish_id,callback){

        //mongoClient.connect("mongodb://"+global.db_server+"/db_hngre",function(err,db){
        //    if(err)
        //    {
        //        console.log(err)
        //    }
        //    else
        //    {
                var merchant = db.collection("merchants")
                var dish_merchant = dish_id.split("_")

                merchant.findOne({"_id":ObjectID(dish_merchant[1]),"dishes":{$elemMatch:{_id: new ObjectID(dish_merchant[0])}}},{"dishes":{$elemMatch:{_id:new ObjectID(dish_merchant[0])}},"info":1},function(err, merchantd){

                    var dish_vector =[]

                    data.kmeans.taste_index.forEach(function (key) {
                        dish_vector.push(parseInt(merchantd.dishes[0].index[key]))
                    })
                    dish_vector.push(parseInt(data.kmeans.cuisines.indexOf(merchantd.dishes[0].cuisine[0])))
                    dish_vector.push(merchantd.info.price_range.length)

                    var cluster_index = data.kmeans.knn_classify.dishes.predict({
                        x : dish_vector,
                        k: data.kmeans.cluster_size.dishes
                    })
                    var dishes =[]
                    var dishes_index = data.kmeans.knn_cluster.dishes.clusters[Math.floor(cluster_index)]
                    dishes_index.forEach(function(dish_index){
                        dishes.push(dish_index)
                    })
                    //db.close()
                    callback(Math.ceil(cluster_index),dishes)

                })

        //    }
        //})

    }

}
