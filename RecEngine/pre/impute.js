//var get_matrix = require("./readmongo.js") // rows as users, dishes as colums
var dataops = require("../helpers/dataops")
var helpers = require("../helpers/cf_helpers")
var data = require("./data")
var predictSimpleSvd = require("../predict/predictSimpleSvd")
var async = require("async")
var svd = require("../algos/svd")
var normalise = require("./norm")
var cf_helpers = require("../helpers/cf_helpers")
var knn = require("../algos/k-means")
var mongoClient = require("mongodb").MongoClient
var global = require("../../Config/global")


function impute(cc,impute_method,rating_matrix1,users,dishes,callback) {
    //console.log("Total %s",rating_matrix1.length)


    var rating_matrix = dataops.copyMatrix(rating_matrix1)

    if (impute_method === "DAVG") // Impute the ratings matrix with Item Average
    {
        log.info("Starting Imputing with DAVG")
        var startime = new Date().getTime()
        var total_clustered = 0

        //for(i=0;i<data.svd.dishes.length;i++)
        //{
        //    var dishavg = data.svd.dish_avgs[i]
        //    for (var u = 0; u < rating_matrix.length; u++) {
        //        if (rating_matrix[u][i] == 0) {
        //            rating_matrix[u][i] = dishavg
        //            if(data.svd.dishes[i] == "552d1649f2f613f72e14e230_543917c0c0e5a407f0f543c7")
        //            {
        //                console.log("hello",dishavg)
        //            }
        //
        //        }
        //    }
        //
        //}
        //var endtime = new Date().getTime()
        //log.info("Finished Imputing with DAVG, took %s seconds", (endtime - startime) / 1000)
        //callback(rating_matrix)

        mongoClient.connect("mongodb://"+global.db_server+"/db_hngre",function(err,db){
            if(err)
            {
                log.info(err)
            }
            else
            {
                log.info("Imputing DAVG, Using Clustering")
                dataops.asyncLoop(dishes.length,function(loop){

                    var dishavg = data.svd.dish_avgs[loop.iteration()]
                    var count = data.svd.dish_count[loop.iteration()]
                    var count_no = 10


                    if(count < count_no )
                    {
                        var dish_avg = 0
                        var inner_count = 0
                        //use other dishes in the same cluster to get a average
                        knn.classify_dishes(db,data.svd.dishes[loop.iteration()],function(cluster_index,dishes){
                            dishes.forEach(function(dish){

                                if(data.svd.dish_count[dish] > count_no )
                                {
                                    dish_avg += data.svd.dish_avgs[dish]
                                    inner_count ++
                                }
                            })
                            var temp = dishavg
                            if( inner_count != 0)
                            {
                                dishavg = (count/count_no)*dishavg + ((count_no-count)/count_no)*(dish_avg/inner_count)
                                total_clustered++
                            }

                            for (var u = 0; u < rating_matrix.length; u++) {
                                if (rating_matrix[u][loop.iteration()] == 0) {
                                    //log.info("Imputed with %s, %s",dishavg,temp)
                                    rating_matrix[u][loop.iteration()] = dishavg

                                }
                            }
                            loop.next()
                        })
                    }
                    else
                    {
                        for (var u = 0; u < rating_matrix.length; u++) {
                            if (rating_matrix[u][loop.iteration()] == 0) {
                                //log.info("Imputed with %s, %s",dishavg)
                                rating_matrix[u][loop.iteration()] = dishavg
                            }
                        }
                        loop.next()

                    }

                },function(){
                    var endtime = new Date().getTime()
                    log.info("Finished Imputing with DAVG, took %s seconds, affected %s dishes", ((endtime - startime) / 1000),total_clustered)
                    db.close()
                    callback(rating_matrix)
                })

            }
        })



    }
    if (impute_method === "UAVG") // Impute the ratings matrix with User Average
    {
        console.log("Starting Imputing with UAVG")
        var startime = new Date().getTime()

        for (i = 0; i < users.length; i++) {
            var useravg = data.user_avgs[i]
            console.log(useravg)
            for (u = 0; u < rating_matrix[0].length; u++) {
                if (rating_matrix[i][u] == 0) {
                    rating_matrix[i][u] = useravg
                }
            }

        }
        var endtime = new Date().getTime()
        console.log("Finished Imputing with UAVG, took %s seconds", (endtime - startime) / 1000)
        callback(rating_matrix)
    }
    if (impute_method === "UANDDAVG") //// Impute the ratings matrix with average of User Average and Item Average
    {
        console.log("Starting Imputing with UANDDAVG")
        var startime = new Date().getTime()

        for(var i=0;i<users.length;i++)
        {
            var useravg1 = dataops.user_avg(rating_matrix,i,users,dishes)
            for(var y=0;y<dishes.length;y++)
            {
                if(rating_matrix[i][y] <=0)
                {
                    rating_matrix[i][y] = (useravg1+ dataops.dish_avg(rating_matrix,y,users,dishes))/2
                }
            }
        }
        var endtime = new Date().getTime()
        console.log("Finishes Imputing with UANDDAVG, took %s seconds",  (endtime - startime) / 1000)



        return rating_matrix

    }

    if(impute_method === "UBIBCF")
    {
        console.log("Starting Imputing with UBIBCF")
        var startime = new Date().getTime()

        var imputed_matrix = impute(cc,"DAVG",rating_matrix,users,dishes,function(imputed_matrix){
            normalise("UAVG",rating_matrix,imputed_matrix,data.svd.users,data.svd.dishes,function(normalised_matrix){
                var svd_imputed = svd(imputed_matrix,data.svd.dim,true)
                var svd_normalised = svd(normalised_matrix,data.svd.dim,true)

                var userspace = svd_imputed.spaces.userspace.elements
                var dishspace = svd_imputed.spaces.dishspace.elements

                var userspace_nn = svd_normalised.spaces.userspace.elements
                var dishspace_nn = svd_normalised.spaces.dishspace.elements

                var toRun = data.svd.users.length
                var final_matrix = []


                var compute_data = {
                    org_matrix : rating_matrix,
                    dishspace_nn : dishspace_nn,
                    userspace_nn : userspace_nn,
                    userspace: userspace,
                    dishspace: dishspace,
                    users : data.svd.users,
                    dishes: data.svd.dishes,
                    user_avgs: data.svd.user_avgs
                }

                for(var user=0;user<data.svd.users.length;user++)
                {
                    cc.enqueue({compute_data:compute_data, user_id:user}, function(err, r) {
                        console.log("Imputed User: %s", r.user_id)
                        final_matrix[r.user_id] = r.imputed_row
                        if (err) console.log("an error occured:", err);

                        if (--toRun == 0){
                            var imputetime_end = new Date().getTime()
                            console.log("DONE")
                            var endtime = new Date().getTime()
                            console.log("Finished Imputing with UBIBCF, took %s seconds", (endtime - startime) / 1000)
                            callback(rating_matrix)
                            cc.exit();

                        }
                    });

                }
            })

        })



        //for(u=0;u<data.users.length;u++)
        //{
        //    var user_avg = data.user_avgs[u]
        //    for(d=0;d<data.dishes.length;d++)
        //    {
        //
        //        var numerator =0
        //        var denomenator =0
        //        var numerator1 =0
        //        var denomenator1 =0
        //
        //        //Item Similarity
        //        for(i=0;i<data.dishes.length;i++)
        //        {
        //            var item_sim = cf_helpers.kItemSim(d,i,dishspace)
        //            //console.log(item_sim)
        //
        //            if(item_sim > 0.5)
        //            {
        //                numerator+= item_sim  * predictSimpleSvd(u,i,userspace,dishspace)
        //                denomenator += Math.abs(item_sim )
        //
        //            }
        //        }
        //
        //
        //        ////User Similarity
        //        for(y=0;y<data.users.length;y++)
        //        {
        //            var user_sim = cf_helpers.kUserSim(u,y,userspace_nn)
        //            //console.log(item_sim)
        //
        //            if(user_sim > 0.5)
        //            {
        //                numerator1+= user_sim  * ((predictSimpleSvd(u,y,userspace,dishspace) - user_avg))
        //                denomenator1 += Math.abs(user_sim )
        //
        //            }
        //        }
        //
        //        var a = 0 ;
        //        var b = 0;
        //        if(numerator!=0 && denomenator!=0)
        //        {
        //            a = numerator/denomenator
        //        }
        //
        //        if(numerator1!=0 && denomenator1!=0)
        //        {
        //            b=0
        //        }
        //
        //
        //        var prediction_isim = user_avg + a
        //        var prediction_usim = user_avg+ b
        //
        //
        //        //var prediction_simple = user_avg + predictSimpleSvd(user_id,dish,userspace,dishspace)
        //        //var final_prediction = predictSimpleSvd(user_id,dish,userspace,dishspace)
        //
        //        var final_prediction = (prediction_isim+prediction_usim)/2
        //
        //        rating_matrix[u][d] = final_prediction
        //        console.log(u,d,final_prediction)
        //    }
        //}


    }

    if (impute_method === "NONE") // Send back original Matrix
    {
        callback(rating_matrix)
    }
}

    //if(impute_method = "UBIBCF")
    //{
    //
    //        console.log('Imputing rating matrix');
    //
    //        //WARNING: This imputation method has been made for a non-normalized matrix.
    //
    //        var user_averages = []
    //        for(u=0;u<users.length;u++)
    //        {
    //            user_averages.push(dataops.user_avg(rating_matrix,u,users,dishes))
    //        }
    //
    //        var imputedMtx = dataops.copyMatrix(rating_matrix);
    //
    //        for (var i = 0; i < rating_matrix.length; i++) { //For each user
    //            for (var j = 0; j < rating_matrix[i].length; j++) { //For each item
    //
    //                if (rating_matrix[i][j] <= 0) {
    //                    //Fill in empty value with UBIBCF.
    //                    var k;
    //
    //                    var numeratorUB = 0;
    //                    var denomUB = 0;
    //
    //                    for (k = 0; k < rating_matrix.length; k++) { //For each user, calculate similarity.
    //                        //Another condition may be added checking whether the user has rated the item in question.
    //
    //                        if (i !== k) {
    //
    //                            var userSimilarity = helpers.kUserSim(i, k, data.userspace);
    //                            if (userSimilarity > 0.5) { //Threshold can be adjusted
    //
    //                                var rating = predictSimpleSvd (k, j, data.userspace, data.dishspace);
    //                                var avg = user_averages[k];
    //
    //                                numeratorUB += userSimilarity * (rating - avg);
    //                                denomUB += userSimilarity;
    //                            }
    //                        }
    //                    }
    //
    //                    var userBasedPred = (numeratorUB / denomUB);
    //                    if (!isFinite(userBasedPred))
    //                    {
    //                        userBasedPred = 0;
    //                    }
    //
    //                    var numeratorIB = 0;
    //                    var denomIB = 0;
    //
    //                    for (k = 0; k < rating_matrix[i].length; k++) { //For each item, calculate similarity.
    //                        //Another condition can be added to check whether this dish has been rated by the user.
    //                        if (j !== k) {
    //                            var itemSimilarity = helpers.kItemSim(j, k, data.dishspace)
    //                            if (itemSimilarity > 0.5) { //Threshold can be adjusted
    //
    //                                var rating = predictSimpleSvd(i, k, data.userspace, data.dishspace);
    //
    //                                numeratorIB += itemSimilarity * rating;
    //                                denomIB += 	itemSimilarity;
    //                            }
    //                        }
    //                    }
    //
    //                    var itemBasedPred = (numeratorIB / denomIB);
    //                    if (!isFinite(itemBasedPred)) itemBasedPred = 0;
    //
    //                    imputedMtx[i][j] = (userBasedPred + itemBasedPred) / 2;
    //
    //                    console.log('Imputed %s,%j with %s', i, j,imputedMtx[i][j] );
    //                }
    //            }
    //        }
    //
    //        return imputedMtx;
    //
    //}
//
//    if(impute_method = "UBIBCFASYNC")
//    {
//
//        console.log('Imputing rating matrix');
//
//        //WARNING: This imputation method has been made for a non-normalized matrix.
//
//        var user_averages = []
//        for(u=0;u<users.length;u++)
//        {
//            user_averages.push(dataops.user_avg(rating_matrix,u,users,dishes))
//        }
//
//        var imputedMtx = dataops.copyMatrix(rating_matrix);
//        var final_matrix = []
//
//        var no_users = users.length
//        var x = no_users
//
//        for(i=0;i<no_users;i++)
//        {
//
//            cc.enqueue(i,imputedMtx,rating_matrix,user_averages,function(err,r){
//                if(err) console.log(err)
//                else
//                {
//                    final_matrix[r.id] = r.imputedrow
//                    no_users--
//                    if(no_users === 0)
//                    {
//                        console.log("All Done")
//                        cc.exit();
//                    }
//
//                }
//
//            })
//
//        }
//
//
//
//
//        //return imputedMtx;
//
//    }
//
//
//}
//
//function ubibfcasync(user_id,imputedMtx,rating_matrix,user_averages){
//
//    console.log(rating_matrix.length)
//
//
//        var i = user_id//For each user
//        for (var j = 0; j < rating_matrix[i].length; j++) { //For each item
//
//            if (rating_matrix[i][j] <= 0) {
//                //Fill in empty value with UBIBCF.
//                var k;
//
//                var numeratorUB = 0;
//                var denomUB = 0;
//
//                for (k = 0; k < rating_matrix.length; k++) { //For each user, calculate similarity.
//                    //Another condition may be added checking whether the user has rated the item in question.
//
//                    if (i !== k) {
//
//                        var userSimilarity = helpers.kUserSim(i, k, data.userspace);
//                        if (userSimilarity > 0.5) { //Threshold can be adjusted
//
//                            var rating = predictSimpleSvd (k, j, data.userspace, data.dishspace);
//                            var avg = user_averages[k];
//
//                            numeratorUB += userSimilarity * (rating - avg);
//                            denomUB += userSimilarity;
//                        }
//                    }
//                }
//
//                var userBasedPred = (numeratorUB / denomUB);
//                if (!isFinite(userBasedPred))
//                {
//                    userBasedPred = 0;
//                }
//
//                var numeratorIB = 0;
//                var denomIB = 0;
//
//                for (k = 0; k < rating_matrix[i].length; k++) { //For each item, calculate similarity.
//                    //Another condition can be added to check whether this dish has been rated by the user.
//                    if (j !== k) {
//                        var itemSimilarity = helpers.kItemSim(j, k, data.dishspace)
//                        if (itemSimilarity > 0.5) { //Threshold can be adjusted
//
//                            var rating = predictSimpleSvd(i, k, data.userspace, data.dishspace);
//
//                            numeratorIB += itemSimilarity * rating;
//                            denomIB += 	itemSimilarity;
//                        }
//                    }
//                }
//
//                var itemBasedPred = (numeratorIB / denomIB);
//                if (!isFinite(itemBasedPred)) itemBasedPred = 0;
//
//                imputedMtx[i][j] = (userBasedPred + itemBasedPred) / 2;
//
//                console.log('Imputed %s,%j with %s', i, j,imputedMtx[i][j] );
//            }
//        }
//
//    return imputedMtx[i]
//
//
//}
module.exports = impute