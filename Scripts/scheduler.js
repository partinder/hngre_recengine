/**
 * Created by Partinder on 4/9/15.
 */

var schedule = require("node-schedule")
var fs = require('fs')
var findRemoveSync = require('find-remove');
var logger = require("winston")
var setup = require("../RecEngine/pre/setup")
var cc = require("../app.js")
var spawn = require('child_process').spawn;
var nodemailer = require("nodemailer")
var data = require("../RecEngine/pre/data")
var fs = require("fs")


// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'hngremailer@gmail.com',
        pass: 'ASDF!#%&'
    }
});

//var rule = new schedule.RecurrenceRule()

// Remove old logs

var j = schedule.scheduleJob("00 59 02 * * *",function(){
    var result = findRemoveSync("logs", {age: {seconds: 864000}});
    logger.info("Old log file removed")

    data.status = "false"

    data.kmeans = {
        cuisines:[],
        taste_index:["sweet","sour","chilly","texture","fat","cookedness","umami"],
        gender:["m","f"],
        vector_dishes:[],
        vector_users:[],
        knn_cluster:{
            users:[],
            dishes:[]
        },
        knn_dishes:[],
        knn_users:[],
        cluster_index:{
            users:[],
            dishes:[]
        },
        cluster_size:{
            dishes:0,
            users:0
        },
        knn_classify:{
            dishes:{},
            users:{}
        },
        status: "false"
    }


    setup("RAW",cc, function(status){
        if(status != true)
        {
            logger.error("SVD not setup")
        }
        else
        {
            logger.info("Setting up Server")

            //Server.route(Routes)
            //
            //Server.start(function(){
            //    var endtime = new Date().getTime()
            //    log.info("Server running at 9500, took:"+(endtime-starttime)/1000+" seconds")
            //    test()
            //})

            var mailOptions = {
                from: 'Hngre Mailer  <hngremailer@gmail.com', // sender address
                to: 'partinder@hngre.com    ', // list of receivers
                subject: 'Recengine Setup', // Subject line
                text: 'RecEngine Setup Sucessfully' // plaintext body
                //html: '<b>Hello world ✔</b>' // html body
            };

// send mail with defined transport object




            logger.info("Server Setup")
            var args = ["service","elasticsearch","restart"]
            var child = spawn("sudo",args)

            child.stdout.on("data",function(data){
                logger.info(data.toString("utf-8"))
            })
            child.stderr.on("data",function(data){
                logger.info("Err : ", data.toString("utf-8"))
            })
            child.on("exit",function(code){
                logger.info("All Done : ",code)
                transporter.sendMail(mailOptions, function(error, info){
                    if(error){
                        return console.log(error);
                    }
                    console.log('Message sent: ' + info.response);

                });
            })

        }

    })

//    var restart_args = ['/opt/node/bin/forever',"restart","app.js"]
//    var forever_cron = spawn("sudo",restart_args )
//
//    forever_cron.stdout.on('data', function(data){
//       // logger.info(data)
//
//    })
//    forever_cron.stderr.on('data', function(data){
//        // Only send error to Logger
//        console.log("Error: %s",data)
//        //logger.error("Error while restarting Server")
//    })
//    forever_cron.on('exit', function(code){
//
//
//// NB! No need to recreate the transporter object. You can use
//// the same transporter object for all e-mails
//
//// setup e-mail data with unicode symbols
//        var mailOptions = {
//            from: 'Hngre Mailer  <hngremailer@gmail.com', // sender address
//            to: 'partinder@hngre.com    ', // list of receivers
//            subject: 'Recengine Setup', // Subject line
//            text: 'RecEngine Setup Sucessfully' // plaintext body
//            //html: '<b>Hello world ✔</b>' // html body
//        };
//
//// send mail with defined transport object
//        transporter.sendMail(mailOptions, function(error, info){
//            if(error){
//                return console.log(error);
//            }
//            console.log('Message sent: ' + info.response);
//
//        });
//
//
//
//        logger.info("Server Setup")
//    })

})


module.exports = j