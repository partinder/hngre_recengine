/**
 * Created by Partinder on 3/12/15.
 */
const computecluster = require('compute-cluster');
var data = require("./../RecEngine/pre/data")
//var setup = require("./RecEngine/pre/setup")
var predictSimpleSvd = require("./RecEngine/predict/predict")
var dataops = require("./../RecEngine/helpers/dataops")
var testdata = require("./RecEngine/helpers/testdata")
var svd = require("./RecEngine/predict/algos/svd")
var numCPUs = require('os').cpus().length;
var normalise = require("./../RecEngine/pre/norm")
var impute = require("./../RecEngine/pre/impute")
var helpers = require("./RecEngine/predict/helpers")
var brain = require("brain")
var _ = require("underscore")


var net = new brain.NeuralNetwork();





var get_matrix_mongo = require("./RecEngine/pre/getmatrix")

// allocate a compute cluster

var dim = 36
var normalization_method = "UAVG"
var impute_method = "DAVG"

get_matrix_mongo("NONE", function(users,dishes,org_matrix){

    //console.log("Number of CPUs: %s",numCPUs)

    //dataops.findNaN(org_matrix)
    var test_data  = testdata(org_matrix,users,dishes,900)

    data.svd.org_matrix_test = test_data.org_matrix

    data.svd.test_set = test_data.test_set

    data.svd.org_matrix = org_matrix

    data.svd.users = users

    data.svd.dishes = dishes

    var rating_matrix = dataops.copyMatrix(data.svd.org_matrix_test)

    var imputed_matrix = impute(impute_method,rating_matrix,data.svd.users,data.svd.dishes)

    //console.log(data.svd.test_set)

    var normalized_matrix = normalise(normalization_method,rating_matrix,imputed_matrix,data.svd.users,data.svd.dishes)


    dataops.findNaN(data.svd.org_matrix_test)

    console.log("Started SVD Normalised")
    var temp = svd(normalized_matrix,dim,true)
    console.log("First Finished")

    data.svd.svd_main = temp.svd_main

    data.svd.userspace = temp.spaces.userspace.elements

    data.svd.dishspace = temp.spaces.dishspace.elements


    console.log("Started SVD Non Normalised")
    var temp_NN = svd(imputed_matrix,dim,true)
    console.log("Finished")

    var userspace_nn = temp_NN.spaces.userspace.elements
    var dishspace_nn = temp_NN.spaces.dishspace.elements

    var error = 0
    var count = 0
    var error_mae = 0

    var error1 = 0
    var count1 = 0
    var error_mae1 = 0
    var training_data =[]

    var toRun = data.svd.users.length
    var final_matrix = []

    var compute_data = {
        org_matrix : dataops.copyMatrix(data.svd.org_matrix_test),
        dishspace_n : dishspace_nn,
        userspace_n : userspace_nn,
        userspace: data.svd.userspace,
        dishspace: data.svd.dishspace,
        users : data.svd.users,
        dishes: data.svd.dishes
    }


    var cc = new computecluster({
        module: './worker.js',
        max_backlog: -1,
        max_processes: Math.ceil(numCPUs*1.75)
    });


    console.log("Start UBIBCF imputing")
    var imptuetime_start = new Date().getTime()

    for(var user=0;user<data.svd.users.length;user++)
    {
            cc.enqueue({compute_data:compute_data, user_id:user}, function(err, r) {
                console.log("Imputed User: %s", r.user_id)
                final_matrix[r.user_id] = r.imputed_row
                if (err) console.log("an error occured:", err);
                if (--toRun == 0){
                    var imputetime_end = new Date().getTime()
                    console.log(" Imputing completed, time take: %s", (imputetime_end-imptuetime_start)/60000)
                    proceedfurther(final_matrix)
                    cc.exit();

                }
            });

    }



    //proceedfurther(imputed_matrix)




})

function proceedfurther(final_matrix){

    var count = 0
    var error = 0
    var error_mae =0
    console.log(final_matrix.length,final_matrix[0].length)
    //

    var normalized_matrix = normalise(normalization_method,data.svd.org_matrix_test,final_matrix,data.svd.users,data.svd.dishes)
    console.log("Started Second SVD")
    var temp1 = svd(normalized_matrix,dim,true)
    console.log("Finished Second SVD")

    var final_userspace = temp1.spaces.userspace.elements

    var final_dishspace = temp1.spaces.dishspace.elements

    //console.log("Started Second SVD NN")
    //var temp2 = svd(final_matrix,dim,true)
    //console.log("Finished Second SVD NN")
    //
    //var final_userspace_n = temp2.spaces.userspace.elements
    //
    //var final_dishspace_n = temp2.spaces.dishspace.elements
    //


    for(s=0;s<data.svd.test_set.length;s++)
    {


            var set = data.svd.test_set[s]
            //var numerator = 0
            //var denomenator = 0
            //
            //var numerator1 = 0
            //var denomenator1 = 0
            var user_avg = dataops.user_avg(data.svd.org_matrix_test,data.svd.users.indexOf(set.user),data.svd.users,data.svd.dishes)
            //
            //
            //
            ////Item Similarity
            //for(i=0;i<data.svd.dishes.length;i++)
            //{
            //    var item_sim = helpers.kItemSim(data.svd.dishes.indexOf(set.dish),i,final_dishspace)
            //    //console.log(item_sim)
            //
            //    if(item_sim > 0.5)
            //    {
            //        numerator+= item_sim  * predictSimpleSvd(data.svd.users.indexOf(set.user),i,final_userspace,final_dishspace)
            //        denomenator += Math.abs(item_sim )
            //
            //    }
            //}
            //
            //
            //////User Similarity
            //for(y=0;y<data.svd.users.length;y++)
            //{
            //    var user_sim = helpers.kUserSim(data.svd.users.indexOf(set.user),y,final_userspace_n)
            //    //console.log(item_sim)
            //
            //    if(user_sim > 0.5)
            //    {
            //        numerator1+= user_sim  * ((predictSimpleSvd(data.svd.users.indexOf(set.user),y,final_userspace,final_dishspace) - user_avg))
            //        denomenator1 += Math.abs(user_sim )
            //
            //    }
            //}
            //
            //var a = 0 ;
            //var b = 0;
            //if(numerator!=0 && denomenator!=0)
            //{
            //    a = numerator/denomenator
            //}
            //
            //if(numerator1!=0 && denomenator1!=0)
            //{
            //    b=0
            //}
            //
            //
            //var prediction_isim = user_avg + a
            //var prediction_usim = user_avg+ b


            //var prediction_simple = user_avg + predictSimpleSvd(user_id,dish,userspace,dishspace)
            //var final_prediction = predictSimpleSvd(user_id,dish,userspace,dishspace)

            //var final_prediction = (prediction_isim+prediction_usim)/2

            var final_prediction = user_avg + predictSimpleSvd(data.svd.users.indexOf(set.user),data.svd.dishes.indexOf(set.dish),final_userspace,final_dishspace)
            console.log("Predicted :%s, Original: %s", Math.ceil(final_prediction), set.rating)

            error += Math.pow(set.rating - Math.ceil(final_prediction), 2);
            error_mae+= Math.abs(set.rating - Math.ceil(final_prediction))
            count++

           //console.log(training_data)
    }

    console.log("RMSE is : %s",Math.sqrt(error / count))
    console.log("MAE is : %s", error_mae/count)








}

