/**
 * Created by Partinder on 3/14/15.
 */

var dataops = require("./dataops")
var helpers = require("./cf_helpers")
var data = require("./../pre/data").svd
var predictSimpleSvd = require("./../predict/predictSimpleSvd")
//var ubibcf = require("./RecEngine/helpers/ubibcf")
//var get_matrix_mongo = require("./RecEngine/pre/getmatrix")

//process.on("message",function(data){
//    process.send(data)
//})


//mongoose.connect("mongodb://localhost/local_ml")

//process.on("message", function(m) {
//    for (var i = 0; i < 100000000; i++);
//    //console.log("Hello")
//
//
//    console.log("received: "+m)
//
//    process.send(5+ m);
//});

process.on("message", function (data){

            var org_matrix = data.compute_data.org_matrix
            var user_id = data.user_id
            var userspace_nn = data.compute_data.userspace_nn
            var userspace = data.compute_data.userspace
            var dishspace = data.compute_data.dishspace
            var users = data.compute_data.users
            var dishes = data.compute_data.dishes
            var user_avg = data.compute_data.user_avgs[user_id]
            //var dish = data.dish_id

            for(var dish=0;dish<org_matrix[user_id].length;dish++)
            {
                if(org_matrix[user_id][dish] <= 0)
                {

                    var numerator = 0
                    var denomenator = 0

                    var numerator1 = 0
                    var denomenator1 = 0



                    //Item Similarity
                    for(i=0;i<dishes.length;i++)
                    {
                        if(i!=dish)
                        {
                            var item_sim = helpers.kItemSim(dish,i,dishspace)
                            //console.log(item_sim)

                            if(item_sim > 0.5)
                            {
                                numerator+= item_sim  * predictSimpleSvd(user_id,i,userspace,dishspace)
                                denomenator += Math.abs(item_sim )

                            }

                        }

                    }


                    ////User Similarity
                    for(y=0;y<users.length;y++)
                    {
                        if(y!=user_id)
                        {
                            var user_sim = helpers.kUserSim(user_id,y,userspace_nn)
                            //console.log(item_sim)

                            if(user_sim > 0.5)
                            {
                                numerator1+= user_sim  * ((predictSimpleSvd(user_id,y,userspace,dishspace) - user_avg))
                                denomenator1 += Math.abs(user_sim )

                            }
                        }

                    }

                    var a = 0 ;
                    var b = 0;
                    if(numerator!=0 && denomenator!=0)
                    {
                        a = numerator/denomenator
                    }

                    if(numerator1!=0 && denomenator1!=0)
                    {
                        b=0
                    }


                    var prediction_isim = user_avg + a
                    var prediction_usim = user_avg+ b
                    //console.log("Predicted :%s, Original: %s", Math.ceil(prediction), set.rating)

                    //var prediction_simple = user_avg + predictSimpleSvd(user_id,dish,userspace,dishspace)
                    org_matrix[user_id][dish] = (prediction_isim+prediction_usim)/2
                    //org_matrix[user_id][dish] = prediction_usim


                }    //console.log(training_data)

        }
    process.send({user_id:user_id,imputed_row:org_matrix[user_id]})
    //mongoose.disconnect()

})


//process.on("message", function (data1){
//    var i = data1.i
//    var imputedMtx = data1.imputedMtx
//    var rating_matrix = data1.org_matrix
//    var user_averages = data1.user_averages
//    var userspace = data1.userspace
//    var dishspace = data1.dishspace
//
//    //console.log(rating_matrix.length)
//    //console.log(i,imputedMtx.length,rating_matrix.length,user_averages.length)
//    for (var j = 0; j < rating_matrix[i].length; j++) { //For each item
//
//        if (rating_matrix[i][j] <= 0) {
//            //Fill in empty value with UBIBCF.
//            var k;
//
//            var numeratorUB = 0;
//            var denomUB = 0;
//
//            for (k = 0; k < rating_matrix.length; k++) { //For each user, calculate similarity.
//                //Another condition may be added checking whether the user has rated the item in question.
//
//                if (i !== k) {
//
//                    var userSimilarity = helpers.kUserSim(i, k, userspace);
//                    if (userSimilarity > 0.5) { //Threshold can be adjusted
//
//                        var rating = predictSimpleSvd (k, j, userspace, dishspace);
//                        var avg = user_averages[k];
//
//                        numeratorUB += userSimilarity * (rating - avg);
//                        denomUB += userSimilarity;
//                    }
//                }
//            }
//
//            var userBasedPred = (numeratorUB / denomUB);
//            if (!isFinite(userBasedPred))
//            {
//                userBasedPred = 0;
//            }
//
//            var numeratorIB = 0;
//            var denomIB = 0;
//
//            for (k = 0; k < rating_matrix[i].length; k++) { //For each item, calculate similarity.
//                //Another condition can be added to check whether this dish has been rated by the user.
//                if (j !== k) {
//                    var itemSimilarity = helpers.kItemSim(j, k, dishspace)
//                    if (itemSimilarity > 0.5) { //Threshold can be adjusted
//
//                        var rating = predictSimpleSvd(i, k, userspace, dishspace);
//
//                        numeratorIB += itemSimilarity * rating;
//                        denomIB += 	itemSimilarity;
//                    }
//                }
//            }
//
//            var itemBasedPred = (numeratorIB / denomIB);
//            if (!isFinite(itemBasedPred)) itemBasedPred = 0;
//
//            imputedMtx[i][j] = (userBasedPred + itemBasedPred) / 2;
//
//            //console.log('Imputed %s,%j with %s', i, j,imputedMtx[i][j] );
//        }
//    }
//
//    process.send({id:i,imputedrow:imputedMtx[i]})
//
//    //process.send({i:i,user_averages:user_averages,org_matrix:rating_matrix,imputedMtx:imputedMtx})
//
//})


