/**
 * Created by Partinder on 3/12/15.
 */

var raw_matrix = require("./readmongo")
var normalise = require("./norm")
var data = require("./data")
var dataops = require("../helpers/dataops")
var svd = require("../algos/svd")
var impute = require("./impute")
var knn = require("../algos/k-means")


//Temp
var get_matrix_mongo = require("../algotest/test_mongo/getmatrix")

module.exports = function(type,cc,callback) {



    if (data.status != true)
    {
        // SVD Setup Start
        if(type == "RAW")
        {
            raw_matrix(function (org_matrix, users, dishes) {


                data.svd.org_matrix = org_matrix

                data.svd.users = users

                data.svd.dishes = dishes

                dataops.dish_avgs(org_matrix,users,dishes)
                dataops.user_avgs(org_matrix,users,dishes)

                var rating_matrix = dataops.copyMatrix(data.svd.org_matrix)
                log.info("Starting Clustering")
                knn.create_clusters(function(status){

                    if(status == true)
                    {

                        impute(cc,data.svd.imputation_method,rating_matrix,data.svd.users,data.svd.dishes,function(imputed_matrix){

                            data.svd.imputed_matrix = dataops.copyMatrix(imputed_matrix)


                            normalise(data.svd.normalization_method,rating_matrix,imputed_matrix,data.svd.users,data.svd.dishes, function(normalized_matrix){

                                dataops.findNaN(data.svd.org_matrix)
                                dataops.findNaN(normalized_matrix)

                                log.info("Started SVD Normalised")
                                var temp = svd(normalized_matrix,data.svd.dim,true)
                                log.info("Finished SVD Normalised")

                                data.svd.svd_main = temp.svd_main

                                data.svd.userspace = temp.spaces.userspace.elements

                                data.svd.dishspace = temp.spaces.dishspace.elements

                                log.info("Start SVD Non_Normalised")
                                var temp1 = svd(imputed_matrix,data.svd.dim,true)
                                log.info("Finished SVD Non_Normalised")

                                data.svd.svd_main_nn = temp1.svd_main

                                data.svd.userspace_nn = temp1.spaces.userspace.elements

                                data.svd.dishspace_nn = temp1.spaces.dishspace.elements

                                log.info("SVD Setup")

                                data.status= true

                                callback(data.status)


                            }) // Normalise


                        }) // Impute

                    }

                }) // Knn

            })

        }
        if(type == "MONGO")
        {
            get_matrix_mongo("NONE", function(org_matrix,users,dishes) {


                data.svd.org_matrix = org_matrix

                data.svd.users = users

                data.svd.dishes = dishes

                var rating_matrix = dataops.copyMatrix(data.svd.org_matrix)

                var imputed_matrix = impute(data.svd.imputation_method,rating_matrix,data.svd.users,data.svd.dishes)

                var normalized_matrix = normalise(data.svd.normalization_method,rating_matrix,imputed_matrix,data.svd.users,data.svd.dishes)

                dataops.findNaN(data.svd.org_matrix)

                console.log("Started SVD Normalised")
                var temp = svd(normalized_matrix,data.svd.dim,true)
                console.log("Finishes SVD Normalised")

                data.svd.svd_main = temp.svd_main

                data.svd.userspace = temp.spaces.userspace.elements

                data.svd.dishspace = temp.spaces.dishspace.elements

                console.log("Start SVD Non_Normalised")
                var temp1 = svd(imputed_matrix,data.svd.dim,true)
                console.log("Finished SVD Non_Normalised")

                data.svd.svd_main_nn = temp1.svd_main

                data.svd.userspace_nn = temp1.spaces.userspace.elements

                data.svd.dishspace_nn = temp1.spaces.dishspace.elements


                console.log("SVD Setup")

                data.status = true


                callback(data.status)

                //})



            })
        }
        //SVD Setup End

    }
    else
    {
        callback(data.status)
    }
}

