/**
 * Created by Partinder on 3/12/15.
 */


module.exports =  {

    svd:
    {
        org_matrix : [],
        org_matrix_test :[],
        imputed_matrix:[],
        normalised_matrix:[],
        user_avgs:[],
        dish_avgs:[],
        user_count:[],
        dish_count:[],
        users:[],
        dishes:[],
        dim :30,
        normalization_method: "UAVG",
        imputation_method: "DAVG",
        svd_main: {},
        svd_main_nn:{},
        userspace: [],
        dishspace:[],
        userspace_nn: [],
        dishspace_nn:[],
        // True if setup is done
        setuptime : null

    },
    kmeans:
    {
        cuisines:[],
        taste_index:["sweet","sour","chilly","texture","fat","cookedness","umami"],
        gender:["m","f"],
        vector_dishes:[],
        vector_users:[],
        knn_cluster:{
            users:[],
            dishes:[]
        },
        knn_dishes:[],
        knn_users:[],
        cluster_index:{
            users:[],
            dishes:[]
        },
        cluster_size:{
            dishes:0,
            users:0
        },
        knn_classify:{
            dishes:{},
            users:{}
        },
        status: "false"
    },
    status:"false",
    test_set : [],
    test_set_size : 100


}
