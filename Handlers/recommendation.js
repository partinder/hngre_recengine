/**
 * Created by Partinder on 4/9/15.
 */

var ubibcfpredict =require("../RecEngine/predict/ubibcfpredict")
var simplesvdpredict = require("../RecEngine/predict/predictSimpleSvd")
var data = require("../RecEngine/pre/data")
var Boom = require("boom")
var dataops = require("../RecEngine/helpers/dataops")
var async = require("async")
var _ =

module.exports = function(req,res) {

    if (data.status != true) {
        res(Boom.badRequest("Server Not SetUp"))
    }
    else {
        console.log(req.payload)
        var startime = new Date().getTime()
        var payload = JSON.parse(req.payload)
        console.log("Received Request")
        var users = []
        var dishid = []
        var user_issue = 0
        var dish_issue = 0
        var reply = {}

        payload.users.forEach(function (user) {
            if (data.svd.users.indexOf(user.userId) == -1) {
                user_issue = 1
                //console.log(user)
            }
            users.push(user)
            //reply[user.userId] = {}
        })

        payload.dishes.forEach(function (dish) {
            if (data.svd.dishes.indexOf(dish.dishId + "_" + dish.merchantId) == -1) {
                dish_issue = 1
                //console.log(dish)
            }
            dishid.push(dish.dishId + "_" + dish.merchantId)
        })


        if (dish_issue == 1)
        {
            res(Boom.notFound("Wrong Dish Id"))
        }
        else {

            var async_func = []
            users.forEach(function (user) {
                async_func.push(
                    function (callback) {
                        callback(null, getRecc(dishid, user))

                    }
                )

            })

            async.parallel(async_func, function (err, results) {
                //console.log(results)
                results.forEach(function(result){
                    //console.log(result)
                    for(property in result) {
                        //console.log(result)
                        reply[property] = result[property]
                    }

                })

                var endtime = new Date().getTime()
                log.info("Predictions took %s Seconds",(endtime-startime)/1000)
                console.log("Response Sent")
                res(reply)
            })


        }


    }
}

function randomise(rating)
{
    if(rating< 1.5)
    {
        // less than 35%
        rating = Math.floor( Math.random()*(35-9+1)+9)
    }
    if(rating > 1.5 && rating < 2)
    {
        //between 35 and 50%
        rating = Math.floor( Math.random()*(50-35+1)+35)

    }
    if(rating > 2 && rating <3)
    {
        // 50 to 65
        rating = Math.floor( Math.random()*(65-50+1)+50)
    }
    //if(rating >3 && rating <3.5)
    //{
    //    //65 to 70%
    //    rating = Math.floor( Math.random()*(70-55+1)+55)
    //
    //}
    if(rating>3.0 && rating < 4.0)
    {
        //70 to 80
        rating = Math.floor( Math.random()*(75-65+1)+65)

    }

    if(rating>4.0 && rating < 4.5)
    {
        //80 to 90
        rating = Math.floor( Math.random()*(90-75+1)+75)

    }
    if(rating>4.5 && rating < 5.0)
    {
        //90 to 98
        rating = Math.floor( Math.random()*(98-90+1)+90)

    }
    return rating

}
function getRecc(dishid,user)
{
    var userid = user.userId
    var  reply ={}
    reply[userid] = {}

    if(data.svd.users.indexOf(userid) == -1)
    {
        log.info("Cant find user %s in SVD Matix, Generating Predictions using Clustering", userid)
        var gender = 0
        //The user is a new user, send Recc's using clustering
        if(user.gender == "male" || user.gender == "m") gender = 1
        var user_vector = [gender, parseInt(user.age)]

        var cluster_index = data.kmeans.knn_classify.users.predict({
            x : user_vector,
            k: data.kmeans.cluster_size.users
        })

        var users_index = data.kmeans.knn_cluster.users.clusters[Math.floor(cluster_index)]

        for(dish = 0; dish<dishid.length; dish++)
        {
            var dishid_rec = [dishid[dish].split("_")[0]]
            var merchantId_rec = dishid[dish].split("_")[1]
            var dish_avg = 0
            var count = 0
            users_index.forEach(function(user_index){

                dish_avg += data.svd.user_avgs[user_index] + simplesvdpredict(user_index,data.svd.dishes.indexOf(dishid[dish]),data.svd.userspace,data.svd.dishspace)
                count ++

            })
            if(reply[userid][merchantId_rec])
            {
                reply[userid][merchantId_rec][dishid_rec] = randomise(dish_avg/count)
                //console.log(randomise(dish_avg/count))
            }
            else
            {
                reply[userid][merchantId_rec] = {}
                reply[userid][merchantId_rec][dishid_rec] = randomise(dish_avg/count)
                //console.log(randomise(dish_avg/count))
            }           

        }
        //console.log(reply)
        return reply


    }
    else
    {
        // User regular SVD reccomendation
        log.info("Found user %s in SVD Matrix, Generating Predictions ise Simple SVD", userid)

        for(dish=0;dish<dishid.length;dish++)
        {
            var dishid_rec = [dishid[dish].split("_")[0]]
            var merchantId_rec = dishid[dish].split("_")[1]
            var rating = data.svd.user_avgs[data.svd.users.indexOf(userid)] + simplesvdpredict(data.svd.users.indexOf(userid),data.svd.dishes.indexOf(dishid[dish]),data.svd.userspace,data.svd.dishspace)
            //rating +=  dataops.user_avg(data.svd.org_matrix,data.svd.users.indexOf(userid),data.svd.users,data.svd.dishes)
            //console.log(data.svd.user_avgs[data.svd.users.indexOf(userid)],simplesvdpredict(data.svd.users.indexOf(userid),data.svd.dishes.indexOf(dishid[dish]),data.svd.userspace,data.svd.dishspace))

            if(reply[userid][merchantId_rec])
            {
                
                reply[userid][merchantId_rec][dishid_rec] =randomise(rating)   
            }
            else
            {
                 reply[userid][merchantId_rec] = {}
                reply[userid][merchantId_rec][dishid_rec] =randomise(rating)   
            }    

        }
        //console.log(reply)
        return reply

    }

}