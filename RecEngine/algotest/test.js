/**
 * Created by Partinder on 6/10/15.
 */


var ubibcfpredict =require("../predict/ubibcfpredict")
var simplesvdpredict = require("../predict/predictSimpleSvd")
var data = require("../pre/data")

module.exports = function(){
    var error = 0
    var count = 0

    data.test_set.forEach(function(set){
        var prediction = data.svd.user_avgs[set[0]] + simplesvdpredict(set[0],set[1],data.svd.userspace,data.svd.dishspace)
        error += Math.pow((prediction-set[2]),2)
        count++
    })
    console.log("RMSE is %s",Math.sqrt((error/count)))

}
