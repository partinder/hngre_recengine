/**
 * Created by Partinder on 4/9/15.
 */
require('newrelic');
var Hapi = require("hapi")
var Routes = require("./Routes/routes")
var Server = new Hapi.Server()
var setup = require("./RecEngine/pre/setup")
var Mongoose = require("mongoose")
GLOBAL.log = require("./Scripts/winston")
var computeCluster  = require("compute-cluster")
var numCPUs = require('os').cpus().length;
var j = require("./Scripts/scheduler") // Setup Scheduler
var global = require("./Config/global")
var data = require("./RecEngine/pre/data")
var test = require("./RecEngine/algotest/test")


var cc = new computeCluster({
    module: './RecEngine/helpers/worker.js',
    max_backlog: -1,
    max_processes: Math.ceil(numCPUs*1.75)
});



if(process.env.NODE_ENV === "production")
{
    log.transports.console.level = 'error';
    log.transports.dailyRotateFile.level = 'error';
    log.transports.logentries.level = "error"
}


Server.connection({
    host:"0.0.0.0",
    port:9500
})

Mongoose.connect("mongodb://"+global.db_server+"/db_hngre")

var starttime = new Date().getTime()

setup("RAW",cc, function(status){
    if(status != true)
    {
        log.error("SVD not setup")
    }
    else
    {
       log.info("Setting up Server")

        Server.route(Routes)

        Server.start(function(){
            var endtime = new Date().getTime()
            log.info("Server running at 9500, took:"+(endtime-starttime)/1000+" seconds")
            test()
        })

    }

})

module.exports = cc

