/**
 * Created by Partinder on 6/3/15.
 */

//Fold in a New user in the SVD

var data = require("../RecEngine/pre/data")
var Boom = require("boom")
var dataops = require("../RecEngine/helpers/dataops")

var sylvester = require('sylvester');
var Matrix = sylvester.Matrix


module.exports = function(req,res){

    if(data.status != true){
        res(Boom.badRequest("Server Not setup"))
    }
    else
    {

        var userId = req.params.userId
        var temp = JSON.parse(req.payload)
        var dishid =[]
        var user_avg = 2

        if(data.svd.users.indexOf(userId) != -1)
        {
            res({success:false,err:"Already Folded in, User Recommend Instead"})
        }

        else
        {

            temp.ratings.forEach(function(dish){
                dishid.push([dish.dishId+"_"+dish.merchantId,dish.rating])
            })

            var T = []

            dishid.forEach(function(dish){
                var count = 0
                var total = 0

                for(i=0;i<data.svd.org_matrix[0].length;i++)
                {
                    if(data.svd.dishes[i] == dish[0])
                    {
                        T[i] = parseInt(dish[1])
                        total += parseInt(dish[1])
                        count++
                    }
                    else
                    {
                        T[i] = data.svd.dish_avgs[i]
                    }

                }
                if(count == 0 && total == 0)
                {
                    user_avg = 2
                }
                else
                {
                    user_avg = total/count
                }




            })

            // Normalise user Vector
            for(i=0;i< T.length;i++)
            {
                T[i] -= user_avg
            }


            T = $M(T).transpose()
            var Vk = $M(data.svd.svd_main.Vt_k).transpose()
            var Skinv = $M(data.svd.svd_main.S_k.elements).inv()

            var T_k = (T.x(Vk)).x(Skinv);


            var Vk_nn = $M(data.svd.svd_main_nn.Vt_k).transpose()

            var Skinv_nn = $M(data.svd.svd_main_nn.S_k.elements).inv()

            var T_k_nn = (T.x(Vk_nn)).x(Skinv_nn);

            if(data.svd.users.indexOf(userId) == -1)
            {

                data.svd.users.push(userId)
                data.svd.user_avgs[data.svd.users.indexOf(userId)] = user_avg

                var U = data.svd.svd_main.U_k;
                var U_nn = data.svd.svd_main_nn.U_k

                U[data.svd.users.indexOf(userId)] = T_k.elements[0];
                U_nn[data.svd.users.indexOf(userId)] = T_k_nn.elements[0];

                data.svd.svd_main.U_k = U;
                data.svd.svd_main_nn.U_k = U_nn;

                // Update Userspace
                var sqrt_S_k = $M(dataops.sqrtmatrix(data.svd.svd_main.S_k.elements));
                var user_space= $M(data.svd.svd_main.U_k).x(sqrt_S_k.transpose())
                data.svd.userspace = user_space.elements


                //Update userspace NN
                var sqrt_S_k_nn = $M(dataops.sqrtmatrix(data.svd.svd_main_nn.S_k.elements));
                var user_space_nn= $M(data.svd.svd_main_nn.U_k).x(sqrt_S_k_nn.transpose())
                data.svd.userspace_nn = user_space_nn.elements

            }

            res({success:true})

        }

    }

}