/**
 * Created by Partinder on 3/9/15.
 */

//var rating_matrix = require("./readmongo.js")
var get_imputed_matrix = require("../../pre/impute")
var mongodb = require("mongodb").MongoClient
var async = require("async")

//module.exports =function(imputation_type,callback){
//    // Get Imputed Matrix saved in Mongo Db
//    mongodb.connect("mongodb://localhost/local_ml", function (err, db) {
//        console.log("Start Reading From Mongo")
//        if (err)console.log(err)
//        async.parallel([
//            function (callback) {
//                var matrix = db.collection("imputed_matrix_" + imputation_type)
//                matrix.findOne({}, function (err, matrix_UAVG) {
//                    if (err)console.log(err)
//                    callback(null, matrix_UAVG)
//                })
//            },
//            function (callback) {
//                var matrix = db.collection("imputed_matrix_NONE")
//                matrix.findOne({}, function (err, matrix_NONE) {
//                    if (err)console.log(err)
//                    callback(null, matrix_NONE)
//                })
//            }
//        ], function (err, result) {
//            var imputed_matrix = result[0].imputed_matrix
//            var users = result[0].users
//            var dishes = result[0].dishes
//            var org_matrix = result[1].imputed_matrix
//
//            //console.log(imputed_matrix_UAVG[0][0],org_matrix[0][0])
//            console.log("Finished Reading from Mongo")
//            callback(imputed_matrix, users, dishes, org_matrix)
//        })
//
//    })
//
//}
//
//module.exports =function(imputation_type,callback){
//   // Get Imputed Matrix saved in Mongo Db
//   mongodb.connect("mongodb://52.5.0.16/db_hngre_ml", function (err, db) {
//       console.log("Start Reading From Mongo")
//       if (err)console.log(err)
//       async.parallel([
//           function (callback) {
//               var matrix = db.collection("org_matrix")
//               matrix.findOne({}, function (err, matrix) {
//                   if (err)console.log(err)
//                   callback(null, matrix)
//               })
//           }
//       ], function (err, result) {
//           //var org = result[0].imputed_matrix
//           var users = result[0].users
//           var dishes = result[0].dishes
//           var org_matrix = result[0].org_matrix
//
//           //console.log(imputed_matrix_UAVG[0][0],org_matrix[0][0])
//           console.log("Finished Reading from Mongo")
//           callback(users, dishes, org_matrix)
//       })
//
//   })
//}

module.exports =function(imputation_type,callback){
     // Get Imputed Matrix saved in Mongo Db
     mongodb.connect("mongodb://localhost/db_hngre_ml", function (err, db) {
         console.log("Start Reading From Mongo")
         if (err)console.log(err)
         async.parallel([
             function (callback) {
                 var matrix = db.collection("org_matrix")
                 matrix.findOne({}, function (err, matrix_NONE) {
                     if (err)console.log(err)
                     callback(null, matrix_NONE)
                 })
             }
         ], function (err, result) {
             //var imputed_matrix = result[0].imputed_matrix
             var users = result[0].users
             var dishes = result[0].dishes
             var org_matrix = result[0].org_matrix

             //console.log(imputed_matrix_UAVG[0][0],org_matrix[0][0])
             console.log("Finished Reading from Mongo")
             callback(org_matrix,users, dishes)
         })

     })

}









