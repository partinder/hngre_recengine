/**
 * Created by Partinder on 1/27/15.
 */


var Mongoose = require("mongoose");
//
//var Schema = Mongoose.Schema
//var Merchant = require("../models/merchant.js")
//db_hngre.model("Merchant", Merchant.schema)



//db_hngre.model(Merchant)
//db_hngre.model("merchant", merchant)



var DishSchema = new Mongoose.Schema({

    name : {type: String, index: true},
    description: String,
    cuisine : {type: [String], index: true},
    sub_cuisine: {type: String, index: true},
    ingredients : {type: [String], index: true},
    course : [String],
    index :{
        sweet : {type:Number, default :1},
        sour: {type:Number, default :1},
        chilly : {type:Number, default :1},
        texture: {type:Number, default :1},
        fat: {type:Number, default :1},
        cookedness: {type:Number, default :1},
        umami: {type:Number, default :1}
    },
    tags: {type: [String], index: true},
    influencers : [String],
    //_merchant : { type: Schema.Types.ObjectId, ref: 'Merchant' },
    photo: {},
    veg_type: Number,
    approve_status: { type: Boolean, default : true}

})

var Dish = Mongoose.model("Dish", DishSchema)

module.exports.Dish = Dish
