/**
 * Created by Partinder on 3/10/15.
 */

var dataops = require("../helpers/dataops")
var mongoClient = require("mongodb").MongoClient
var data = require("./data")
var global = require("../../Config/global")
var knn = require("../algos/k-means")

module.exports = function(normalization_method,org_matrix,imputed_matrix1,users,dishes,callback){


    var imputed_matrix = dataops.copyMatrix(imputed_matrix1)

    if(normalization_method === "DAVG"){
        var startime = new Date().getTime()
        log.info("Normalization Started")

        for(i=0;i<dishes.length;i++)
        {
            var dish_av = dataops.dish_avg(org_matrix,i,users,dishes)
            for(u=0;u<users.length;u++)
            {
                imputed_matrix[u][i] -= dish_av
            }
        }
        var endtime = new Date().getTime()
        log.info("Finished, Normalization took: %s seconds", (endtime-startime)/1000)
        return imputed_matrix


    }

    if(normalization_method === "UAVG"){
        var startime = new Date().getTime()
        var total_clustered = 0

        log.info("Normalization Started")

        for(var i=0;i<users.length;i++)
        {
            var user_av = data.svd.user_avgs[i]
            for(var u=0;u<dishes.length;u++)
            {
                imputed_matrix[i][u] -= user_av
            }
        }

        var endtime = new Date().getTime()
        log.info("Finished, Normalization took: %s seconds", (endtime-startime)/1000)
        //console.log("Users are %s, dishes are :%s", imputed_matrix.length, imputed_matrix[0].length)
        callback(imputed_matrix)


        //mongoClient.connect("mongodb://"+global.db_server+"/db_hngre",function(err,db){
        //    if(err)
        //    {
        //        log.info(err)
        //    }
        //    else
        //    {
        //        log.info("Started Normalization with UAVG, Clustered")
        //        dataops.asyncLoop(users.length,function(loop){
        //
        //            var useravg = data.svd.user_avgs[loop.iteration()]
        //            var temp = useravg
        //            var count = data.svd.user_count[loop.iteration()]
        //            var count_no = 10
        //
        //
        //
        //            if(count < count_no )
        //            {
        //                var user_avg = 0
        //                var inner_count = 0
        //                //use other dishes in the same cluster to get a average
        //                knn.classify_users(db,data.svd.users[loop.iteration()],function(cluster_index,users){
        //
        //                    if(cluster_index == null || users == null)
        //                    {
        //                        for (var u = 0; u < imputed_matrix[0].length; u++) {
        //                                //log.info("Normalaised with %s, %s",useravg,temp)
        //                                imputed_matrix[loop.iteration()][u] -= useravg
        //                        }
        //                        loop.next()
        //
        //                    }
        //                    else
        //                    {
        //                        users.forEach(function(user){
        //
        //                            if(data.svd.user_count[user] > count_no )
        //                            {
        //                                user_avg += data.svd.user_avgs[user]
        //                                inner_count ++
        //                            }
        //                        })
        //
        //                        if( inner_count != 0)
        //                        {
        //
        //                            useravg = (count/count_no)*useravg + ((count_no-count)/count_no)*(useravg/inner_count)
        //                            total_clustered ++
        //                            //log.info("Using clustered for User : %s", data.svd.users[loop.iteration()])
        //                        }
        //
        //                        for (var u = 0; u < imputed_matrix[0].length; u++)
        //                        {
        //                                //log.info("Normalised with %s, %s",useravg,temp)
        //                                imputed_matrix[loop.iteration()][u] -= useravg
        //                        }
        //                        loop.next()
        //
        //                    }
        //
        //                })
        //            }
        //            else
        //            {
        //               //log.info("Normal UAVG : %s", useravg)
        //                for (var u = 0; u < imputed_matrix[0].length; u++)
        //                {
        //                        //log.info("Normalised with %s, %s",useravg,temp)
        //                        imputed_matrix[loop.iteration()][u] -= useravg
        //                }
        //                loop.next()
        //
        //            }
        //
        //        },function(){
        //            var endtime = new Date().getTime()
        //            log.info("Finished Normalization with UAVG, Clustered took %s seconds. Affected %s users", ((endtime - startime) / 1000),total_clustered)
        //            db.close()
        //            callback(imputed_matrix)
        //        })
        //
        //    }
        //})




    }
}
