/*
 DATA-OPS
 Contains the various operations to transform the data as needed.
 1. Calculating square root of a matrix.
 2. Creating a copy of a matrix.
 3. Logging the difference between matrices.
 4. Shuffling arrays.
 */

require('sylvester');
var data = require("../pre/data")

module.exports = {

    /**
     * Shuffles elements of an array
     * @param array
     * @returns {*}
     */
    shuffle: function (array) {

        var currentIndex = array.length, temporaryValue, randomIndex ;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    },

    /**
     * Generates a new matrix with square root elements of a given matrix.
     * @param mat
     * @returns {Array|*}
     */
    sqrtmatrix: function (mat)
    {
        var modMtx = mat.map(function (arr) {
            return arr.map(Math.sqrt);
        });

        return modMtx;
    },

    /**
     * Generates a clone of a given matrix.
     * @param mat
     * @returns {Array|*}
     */
    copyMatrix: function (mat) {

        var modMtx = mat.map(function(arr) {
            return arr.slice();
        });

        return modMtx;
    },

    mtxDiffs: function (mtx1, mtx2) { //Logs the elements of matrices, if different

        for (var i = 0; i < mtx1.length; i++) {
            for (var j = 0; j < mtx1[i].length; j++) {
                if (mtx1[i][j] !== mtx2[i][j]) {
                    console.log('Index', i, ',', j, 'values', mtx1[i][j], mtx2[i][j]);
                }
            }
        }
    },

    /**
     * Converts a singular value array into a diagonal matrix.
     * @param arr
     * @returns {Array}
     */
    diagonalMatrix: function  (arr) {

        var matrix = [];

        //Converting singular value array into a diagonal matrix.
        for (var i = 0; i < arr.length; i++) {

            for (var j = 0; j < arr.length; j++) {

                if (matrix[i] === undefined) {
                    matrix[i] = [];
                }

                if (i === j) {
                    matrix[i][j] = arr[i];
                } else {
                    matrix[i][j] = 0;
                }
            }
        }

        return matrix;
    },

    user_avg : function(org_matrix,user,users,dishes){
        var total = 0
        var count = 0
        //console.log(user,org_matrix[user].length)
        for(i=0;i<dishes.length;i++)
        {
            if(org_matrix[user][i] > 0)
            {
                total += org_matrix[user][i]
                count ++
            }
        }

        if(isNaN(total) || isNaN(count))
        {
            throw "Either Total or Count NAN"
        }

        if(total == 0 && count == 0)
        {
            //console.log("Total 0 and Count 0, imputing with average")
            return 2
        }

    return (total/count)
    },

    dish_avg: function (org_matrix,dish,users,dishes){

        var total = 0
        var count = 0

        for(var i=0;i < users.length;i++)
        {
            if( org_matrix[i][dish] > 0)
            {
                total += org_matrix[i][dish]
                count ++
            }
            //console.log(total,count,dishes[dish])
        }

        if(isNaN(total) || isNaN(count))
        {
            throw "Either Total or Count NAN"
        }
        if(total == 0 && count == 0)
        {
            //console.log("Total 0 and Count 0, imputing with average")
            return 2
        }

    return (total/count)
    },

    checkRMSE : function(svd, ratings) {

    var error = 0;
    var count = 0;

    ratings.forEach(function (rating) {

        var userIndex = index.forUser(rating.u);
        var itemIndex = index.forItem(rating.i);
        var actualRating = rating.r;

        var predictedRating = predict.predictSimple(userIndex, itemIndex, svd.tempU, svd.tempV);

        error += Math.pow(actualRating - predictedRating, 2);
        count++;
    });

    return Math.sqrt(error / count);
},
    findNaN : function(matrix){
        for(i=0;i<matrix.length;i++)
        {
            for(u=0;u<matrix[i].length;u++)
            {
                if(isNaN(matrix[i][u]))
                {
                    throw "NAN in Main Matrix"
                }
            }
        }
    },

    dish_avgs : function(org_matrix,users,dishes){

        for(d=0;d<org_matrix[0].length;d++)
        {
            var count =0
            var total = 0
            for(u=0;u<org_matrix.length;u++)
            {
                if( org_matrix[u][d] > 0)
                {
                    total += org_matrix[u][d]
                    count ++
                }

            }
            if(isNaN(total) || isNaN(count))
            {
                throw "Either Total or Count NAN"
            }

            if(total == 0 && count == 0)
            {
                //console.log("Total 0 and Count 0, imputing with average")
                data.svd.dish_avgs[d] = 2
                data.svd.dish_count[d] = 0
            }
            else
            {
                data.svd.dish_avgs[d] = total/count
                data.svd.dish_count[d] = count
            }

        }

    },
    user_avgs : function(org_matrix,users,dishes){


        for(u=0;u<org_matrix.length;u++)
        {
            var count =0
            var total = 0
            for(i=0;i<org_matrix[0].length;i++)
            {
                if( org_matrix[u][i] > 0)
                {
                    total += org_matrix[u][i]
                    count ++
                }

            }
            if(isNaN(total) || isNaN(count))
            {
                throw "Either Total or Count NAN"
            }

            if(total == 0 && count == 0)
            {
                //console.log("Total 0 and Count 0, imputing with average")
                data.svd.user_avgs[u] = 2
                data.svd.user_count[u]= 0
            }
            else
            {
                data.svd.user_avgs[u] = total/count
                data.svd.user_count[u] = count
            }

        }

    },
    asyncLoop: function (iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}

}