/**
 * Created by Partinder on 4/9/15.
 */
var Joi = require("joi")
var data = require("../RecEngine/pre/data")
var recommendation_handler = require("../Handlers/recommendation")
var foldin_handler = require("../Handlers/foldIn")
var fs = require("fs")

module.exports =[

    {
        method: "GET",
        path:"/status",
        handler: function(req,res){

            if(data.status == true)
            {
                res({success:true})
            }

            else
            {
                res({success:false})
            }
        },
        config:{

        }

    },
    {
        method:"POST",
        path:"/recommend/{userId?}",
        handler :recommendation_handler,
        config:{
            validate:{
                //params: {
                //    userId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
                //},
                //payload: Joi.array().items(Joi.object().keys(
                //        {
                //            dishId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
                //            merchantId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
                //        }).and('dishId', 'merchantId')
                //)
            }
        }
    },
    {
        method : "POST",
        path:"/update/{userId?}",
        handler : foldin_handler,
        config:{
            // Ratings can be new for old user or new user with new ratings
            validate:{
                //payload: Joi.object().keys({
                //    dishId: Joi.array().items(Joi.string().regex(/^[0-9a-fA-F]{24}$/)).single()
                //}),
                params:{
                    userId:Joi.string().regex(/^[0-9a-fA-F]{24}$/)
                }
            }
        }
    },
    // Testing Loader.op
    {
        method : "GET",
        path:"/loaderio-c2fe8ff796eea419c8aae54f01f2ec3c/",
        handler:function(req,res){
            var file = fs.readFileSync("./loaderio-c2fe8ff796eea419c8aae54f01f2ec3c.txt")
            res(file)
        },
        config:{

        }
    }


]