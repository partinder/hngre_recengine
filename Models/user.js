var Mongoose = require ('mongoose');
var winston = require('winston');

var LoginDataSchema = new Mongoose.Schema({
	type : String,
	date : Date,
	data : {}
}, {_id : false});

var UserSchema = new Mongoose.Schema({

	//name: String,
	//first_name: String,
	//last_name: String,
	//email: {type: String, index: {unique: true, required: true}},
	//birthday: Date,
	//gender: String,
	//loc: Array,
	//veg_scale: {type: Array, default: [1,2,3]},
	//login: [LoginDataSchema],
	//created: {type: Date, default: Date.now}
});

/**
 * Creates or updates profile. Convenience method for logging in with social media.
 * @param user
 * @param callback
 */
UserSchema.statics.createOrUpdate = function (user, callback)
{
	if (user.email) 
	{
		User.findOneAndUpdate({email: user.email}, user, function (err, foundUser) 
		{
			if (err) {

				return callback (err);

			} else {

				if (foundUser) {

                    _.extend(foundUser, user);

                    foundUser.save(function (err) {

                        if (err) {
                            winston.error('Error updating user', {id: foundUser._id});
                        }

                        callback(null, foundUser);

                    });

				} else {

					var newUser = new User(user);

					winston.info('Creating new user ', newUser);

					newUser.save(function (err, savedUser) {
						if (err) {

                            winston.error('Error creating new user', {email: user.email});
							return callback (err);

						} else {

							var returnUser = savedUser;
							callback (null, savedUser);
						}
					});
				}
			}
		});
	}
	else {
		callback (new Error('Email ID is missing'));
	}
}

UserSchema.statics.createNew = function (user, callback)
{
	console.log('inserting: ', user);
	var newUser = new this(user); //TODO Test this
	newUser.save(function (err, user)
	{
		if (err){
			console.error(err);

			var errMsg;
			errMsg.status = 0;

			if (err.name === 'ValidationError')
			{
				
			}

			return callback (err);
		} 
		return callback (null, user);
	});
}

UserSchema.statics.updateDetails = function (user, callback) {

	if (!user._id) {
		callback (new Error('ID is required to update details'));

	} else {

		this.findOneAndUpdate(user._id, user, function (err, user) {
			if (err) {
				return callback (err);
			} else {
				return callback (null, user);
			}
		});
	}

}
var User = Mongoose.model('User', UserSchema);

module.exports = User;
