/**
 * Created by Partinder on 4/9/15.
 */
var data = require("../pre/data").svd
var dataops = require("../helpers/dataops")
var predictSimpleSvd = require("./predictSimpleSvd")
var cf_helpers = require("../helpers/cf_helpers")

module.exports = function (userid,dishid) {

    var user_avg = data.user_avgs[data.users.indexOf(userid)]

    var numerator =0
    var denomenator =0
    var numerator1 =0
    var denomenator1 =0

    //Item Similarity
    for(i=0;i<data.dishes.length;i++)
    {
        var item_sim = cf_helpers.kItemSim(data.dishes.indexOf(dishid),i,data.dishspace)
        //console.log(item_sim)

        if(item_sim > 0.5)
        {
            numerator+= item_sim  * predictSimpleSvd(data.users.indexOf(userid),i,data.userspace,data.dishspace)
            denomenator += Math.abs(item_sim )

        }
    }


    ////User Similarity
    for(y=0;y<data.users.length;y++)
    {
        var user_sim = cf_helpers.kUserSim(data.users.indexOf(userid),y,data.userspace_nn)
        //console.log(item_sim)

        if(user_sim > 0.5)
        {
            numerator1+= user_sim  * ((predictSimpleSvd(data.users.indexOf(userid),y,data.userspace,data.dishspace) - user_avg))
            denomenator1 += Math.abs(user_sim )

        }
    }

    var a = 0 ;
    var b = 0;
    if(numerator!=0 && denomenator!=0)
    {
        a = numerator/denomenator
    }

    if(numerator1!=0 && denomenator1!=0)
    {
        b=0
    }


    var prediction_isim = user_avg + a
    var prediction_usim = user_avg+ b


    //var prediction_simple = user_avg + predictSimpleSvd(user_id,dish,userspace,dishspace)
    //var final_prediction = predictSimpleSvd(user_id,dish,userspace,dishspace)

    var final_prediction = (prediction_isim+prediction_usim)/2

    return final_prediction

    //var final_prediction = user_avg + predictSimpleSvd(data.svd.users.indexOf(set.user),data.svd.dishes.indexOf(set.dish),final_userspace,final_dishspace)
    //console.log("Predicted :%s, Original: %s", Math.ceil(final_prediction), set.rating)
    //
    //error += Math.pow(set.rating - Math.ceil(final_prediction), 2);
    //error_mae+= Math.abs(set.rating - Math.ceil(final_prediction))
    //count++

    //console.log(training_data)
}
