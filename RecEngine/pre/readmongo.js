/**
 * Created by Partinder on 3/2/15.
 */
var rating = require("../../Models/ratingm.js")
var merchant = require("../../Models/merchant.js").merchant
var _ = require("underscore")
var data = require("./data")
var user = require("../../Models/user")
var dumy_user = require("../../Models/dumy_user")

// Change the Model when using dish data.


var mongoose = require("mongoose")
var async = require("async")

var matrix = []
var dishes = []
var users = []
var users_rating = []
    //mongoose.connect("mongodb://52.1.143.142/db_hngre_ml")
    //mongoose.connect("mongodb://localhost/db_hngre_ml")

module.exports = function(callback) {

    log.info("Reading Ratings Collection")

    var starttime = new Date().getTime()

    async.parallel([

        function(callback) {

            var users = []
            user.find({
                "birthday": {
                    $ne: null
                },
                "gender": {
                    $ne: null
                }
            }, {
                "gender": 1,
                "birthday": 1
            }, function(err, res) {
                if (err) throw err
                    //console.log("Found Users")
                res.forEach(function(user) {
                    users.push(user._id)
                })
                dumy_user.find({
                    "age": {
                        $exists: true
                    },
                    "gender": {
                        $exists: true
                    }
                }, {
                    "age": 1,
                    "gender": 1
                }, function(err, res) {
                    if (err) throw err
                        //console.log("Found Dumy Users")
                    res.forEach(function(user) {
                        users.push(user._id)
                    })
                    users_rating = users
                    rating.find({
                        "_user": {
                            $in: users
                        },
                        "rating": {
                            $exists: true
                        }
                    })
                        .distinct("_user")
                        .exec(function(err, users_mongo) {
                            if (err) {
                                console.log("mongoose", err)
                            } else {
                                //console.log(users_mongo.length)

                                callback(null, users_mongo)
                            }
                        })
                })
            })
        },
        function(callback) {
            merchant.find({
                "status": "completed"
            })
                .select("dishes name")
                .exec(function(err, dishes_mongo) {
                    if (err) {

                    } else {
                        callback(null, dishes_mongo)
                    }
                })
        }
    ], function(err, results) {
        if (err) {

        } else {

            var users = results[0]
            var dishes = []

            results[1].forEach(function(mer) {
                mer.dishes.forEach(function(dish) {
                    dishes.push(dish._id.toString() + "_" + mer._id.toString())
                })

            })
            //console.log(dishes)
            log.info("Total dishes are: %s", dishes.length)
            log.info("Total users are: %s", users.length)
            for (a = 0; a < users.length; a++) {
                users[a] = users[a].toString()
            }
            for (b = 0; b < dishes.length; b++) {
                dishes[b] = dishes[b].toString()
            }

            matrix = createMatrix(users, dishes)
            var test_set = []

            rating.find({
                "_user": {
                    $in: users_rating
                },
                "rating": {
                    $exists: true
                }
            })
                .exec(function(err, ratings) {
                    if (err) {
                        console.log(err)

                    } else {
                        log.info("Total ratings are: %s", ratings.length)
                        //console.log(ratings)
                        var temp = []
                        ratings.forEach(function(ratingmon) {
                            ratingmon = ratingmon.toObject()
                            var userindex = users.indexOf(ratingmon._user.toString())
                                //console.log(dishes.indexOf(ratingmon._dish.toString()+"_"+ratingmon._merchant.toString()))
                            var dishindex = dishes.indexOf(ratingmon._dish.toString() + "_" + ratingmon._merchant.toString())
                            if (dishindex == -1) {
                                console.log(ratingmon._dish.toString() + "_" + ratingmon._merchant.toString())
                            }

                            //console.log(userindex)

                            matrix[userindex][dishindex] = parseInt(ratingmon.rating)
                            test_set.push([userindex, dishindex, parseInt(ratingmon.rating)])
                            temp.push(userindex + dishindex + ratingmon.rating.toString)
                            if (temp.indexOf(userindex.toString() + dishindex.toString() + ratingmon.rating.toString()) != -1) {
                                console.log(userindex, dishindex, ratingmon.rating.toString())
                                //console.log("reptotion")
                            }

                        })
                        log.info("Creating Test Set")
                        matrix = create_test_set(test_set, matrix)
                        log.info("Done test set")

                        log.info("Finished Reading, Matrix Created")
                        callback(matrix, users, dishes)
                    }
                })

        }
    })


}

function createMatrix(users, dishes) {

    matrix = Array(users.length)
    for (u = 0; u < users.length; u++) {
        matrix[u] = Array(dishes.length)
    }

    for (x = 0; x < users.length; x++) {
        for (y = 0; y < dishes.length; y++) {
            matrix[x][y] = 0
        }
    }
    return matrix
}

function create_test_set(test_set, matrix) {
    var random_index = []
    while (random_index.length != data.test_set_size) {
        var new_ran = Math.ceil(Math.random() * test_set.length)
        if (random_index.indexOf(new_ran) == -1) {
            random_index.push(new_ran)
        }
    }
    random_index.forEach(function(index) {
        data.test_set.push(test_set[index])
        var temp = test_set[index]
        var user_index = temp[0]
        var dish_index = temp[1]
        matrix[user_index][dish_index] = 0
    })
    //console.log(data.test_set)
    return matrix
}