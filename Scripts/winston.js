/**
 * Created by Partinder on 4/9/15.
 */
var winston = require("winston")
var Logentries = require('winston-logentries');
var keys = require("../Config/keys")

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({handleExceptions:true}),
        new (winston.transports.DailyRotateFile)({ filename: 'logs/server.log', handleExceptions: true, datePattern:".dd-MM-yyyy" }),
        new (winston.transports.Logentries)({token: keys.logentries, handleExceptions: true})
    ]
});

logger.transports.console.level = 'info';
logger.transports.dailyRotateFile.level = 'info';
logger.transports.logentries.level = "silent"

// Set Levels based on environment



//logger.transports.console.level = 'silly';
//logger.transports.dailyRotateFile.level = 'silly';
//logger.transports.logentries.level = "error"

// Log Levels

//Lowest - silly
//
//verbose
//info
//http
//warn
//error

//Highest - silent

module.exports = logger