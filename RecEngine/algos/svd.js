/**
 * Created by Partinder on 3/3/15.
 */
var svd = require("node-svd").svd
var Matrix = require("sylvester").Matrix
require("sylvester")

var dataops = require("../helpers/dataops")

var _ = require("underscore")

module.exports = function(matrix,dim,status){

    if(status != true){

        log.info("Setup not Complete, Setup Again")
    }
    else
    {
            var settings = {
                debug: 0
            }
            //console.log(matrix.length,matrix[0].length)
            var svd_obj = svd(matrix, dim, settings)


            var U_k = svd_obj.U
            var Vt_k = svd_obj.V

            var S_k = Matrix.Diagonal(svd_obj.S);

            var sqrt_S_k = $M(dataops.sqrtmatrix(S_k.elements));

            var userspace = $M(U_k).x(sqrt_S_k.transpose())
            var dishspace = sqrt_S_k.x($M(Vt_k))


            return {svd_main:{U_k:U_k,Vt_k:Vt_k,S_k:S_k},spaces:{userspace:userspace,dishspace:dishspace}}
    }

}











